import { createBrowserRouter } from "react-router-dom";
import { Basket } from "./pages/Basket";
import { Favorites } from "./pages/Favorites";
import { Main } from "./pages/Main";
import { Store } from "./pages/Store";
import { ErrorPage } from './pages/ErrorPage';

export const router = createBrowserRouter([{
    path: '/',
    element: <Main />,
    errorElement:<ErrorPage/>,
    children: [
        {
            path: '/',
            element: <Store />
        }, {
            path: '/basket',
            element: <Basket />
        }, {
            path: '/favorites',
            element: <Favorites />
        }
    ]
}])
