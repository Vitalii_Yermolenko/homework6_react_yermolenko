import { formReducer } from './formReducer';
import { OPEN_FORM, CLOSE_FORM, CLOSE_FORM_ONLY } from '../actions';

describe('formReducer', () => {
  it('should return the initial state', () => {
    expect(formReducer(undefined, {})).toEqual({ currentData:[], formVisible:false });
  });

  it('should handle OPEN_FORM', () => {
    expect(
      formReducer(undefined, {
        type: OPEN_FORM
      })
    ).toEqual({ currentData:[], formVisible:true });
  });

  it('should handle CLOSE_FORM', () => {
    expect(
      formReducer(undefined, {
        type: CLOSE_FORM,
        payload: {
          currentData: { name: 'test product', price: 10 }
        }
      })
    ).toEqual({ currentData:{ name: 'test product', price: 10 }, formVisible:false });
  });

  it('should handle CLOSE_FORM_ONLY', () => {
    expect(
      formReducer(undefined, {
        type: CLOSE_FORM_ONLY
      })
    ).toEqual({ currentData:[], formVisible:false });
  });
});