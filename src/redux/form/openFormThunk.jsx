

import { OPEN_FORM } from "../actions"

export function openFormThunk(){
    return function(dispatch){
        dispatch({type: OPEN_FORM});
    }
}