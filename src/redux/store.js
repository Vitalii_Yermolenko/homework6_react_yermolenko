import { combineReducers, createStore,applyMiddleware, compose } from 'redux'
import thunk from 'redux-thunk'
import { favoritesReducer } from './favorites/favoritesReducer'
import { productsReducer } from './products/productsReducer'
import { basketReducer } from './basket/basketReducer'
import { modalReducer } from './modal/modalReducer'
import { formReducer } from './form/formReducer'




const rootReducer = combineReducers({
    favorites: favoritesReducer,
    basket: basketReducer,
    products: productsReducer,
    modal : modalReducer,
    form: formReducer
})

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

export const store = createStore(
  rootReducer,
  composeEnhancers(applyMiddleware(thunk))
);