import { productsReducer } from './productsReducer';
import { GET_PRODUCTS_REQUEST, GET_PRODUCTS_SUCCSES, GET_PRODUCTS_ERROR, CHANGE_CHOOSE_PRODUCT } from '../actions';

describe('productsReducer', () => {
  it('should return the initial state', () => {
    expect(productsReducer(undefined, {})).toEqual({
      products: [],
      isLoading: false,
      chooseProduct: null
    });
  });

  it('should handle GET_PRODUCTS_REQUEST', () => {
    expect(
      productsReducer(undefined, {
        type: GET_PRODUCTS_REQUEST
      })
    ).toEqual({
      products: [],
      isLoading: true,
      chooseProduct: null
    });
  });

  it('should handle GET_PRODUCTS_SUCCSES', () => {
    const products = [
      { id: 1, name: 'Product 1' },
      { id: 2, name: 'Product 2' }
    ];
    expect(
      productsReducer(undefined, {
        type: GET_PRODUCTS_SUCCSES,
        payload: { products }
      })
    ).toEqual({
      products,
      isLoading: false,
      chooseProduct: null
    });
  });

  it('should handle GET_PRODUCTS_ERROR', () => {
    expect(
      productsReducer(undefined, {
        type: GET_PRODUCTS_ERROR
      })
    ).toEqual({
      products: [],
      isLoading: false,
      chooseProduct: null
    });
  });

  it('should handle CHANGE_CHOOSE_PRODUCT', () => {
    const chooseProduct = { id: 1, name: 'Product 1' };
    expect(
      productsReducer(undefined, {
        type: CHANGE_CHOOSE_PRODUCT,
        payload: { chooseProduct }
      })
    ).toEqual({
      products: [],
      isLoading: false,
      chooseProduct
    });
  });
});