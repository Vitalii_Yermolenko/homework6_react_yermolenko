import { GET_PRODUCTS_REQUEST,GET_PRODUCTS_SUCCSES,GET_PRODUCTS_ERROR, CHANGE_CHOOSE_PRODUCT } from "../actions"

export function productsReducer(state = {products:[], isLoading:false, chooseProduct:null}, action){
    switch (action.type){
        case GET_PRODUCTS_REQUEST:
            return {...state, isLoading:true}
        case GET_PRODUCTS_SUCCSES:
            return {...state, products:action.payload.products, isLoading:false}
        case GET_PRODUCTS_ERROR:
            return {...state, isLoading:false}
        case CHANGE_CHOOSE_PRODUCT:
            return {...state, chooseProduct:action.payload.chooseProduct }
        default:
            return state
    }
}