import { favoritesReducer } from './favoritesReducer';
import { TOGGLE_PRODUCT_TO_FAVORITES_ACTION_TYPE } from '../actions';

describe('favoritesReducer', () => {
  it('should toggle product to favorites list', () => {
    const initialState = [1, 2, 3];
    const action = {
      type: TOGGLE_PRODUCT_TO_FAVORITES_ACTION_TYPE,
      payload: { productId: 2 },
    };

    const newState = favoritesReducer(initialState, action);

    expect(newState).toEqual([1, 3]);
  });

  it('should add product to favorites list', () => {
    const initialState = [1, 2];
    const action = {
      type: TOGGLE_PRODUCT_TO_FAVORITES_ACTION_TYPE,
      payload: { productId: 3 },
    };

    const newState = favoritesReducer(initialState, action);

    expect(newState).toEqual([1, 2, 3]);
  });

  it('should remove product from favorites list', () => {
    const initialState = [1, 2, 3];
    const action = {
      type: TOGGLE_PRODUCT_TO_FAVORITES_ACTION_TYPE,
      payload: { productId: 3 },
    };

    const newState = favoritesReducer(initialState, action);

    expect(newState).toEqual([1, 2]);
  });

  it('should return the initial state if action type is unknown', () => {
    const initialState = [1, 2, 3];
    const action = { type: 'UNKNOWN_ACTION' };

    const newState = favoritesReducer(initialState, action);

    expect(newState).toEqual(initialState);
  });
});