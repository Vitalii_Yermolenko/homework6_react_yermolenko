import { ADD_PRODUCT_TO_BASKET_ACTION_TYPE, REMOVE_PRODUCT_TO_BASKET_ACTION_TYPE, DELETE_PRODUCT_IN_BASKET } from "../actions";
import { basketReducer } from "./basketReducer";

describe("basketReducer", () => {
  let initialState;

  beforeEach(() => {
    initialState = {};
  });

  it("should return the initial state", () => {
    const newState = basketReducer(undefined, {});
    expect(newState).toEqual(initialState);
  });

  it("should add a product to the basket", () => {
    const productId = "1";
    const newState = basketReducer(initialState, { type: ADD_PRODUCT_TO_BASKET_ACTION_TYPE, payload: { productId } });
    expect(newState).toEqual({ [productId]: 1 });
  });

  it("should increment the quantity of an existing product in the basket", () => {
    const productId = "1";
    const state = { [productId]: 2 };
    const newState = basketReducer(state, { type: ADD_PRODUCT_TO_BASKET_ACTION_TYPE, payload: { productId } });
    expect(newState).toEqual({ [productId]: 3 });
  });

  it("should remove a product from the basket", () => {
    const productId = "1";
    const state = { [productId]: 1 };
    const newState = basketReducer(state, { type: REMOVE_PRODUCT_TO_BASKET_ACTION_TYPE, payload: { productId } });
    expect(newState).toEqual({});
  });

  it("should delete all products from the basket", () => {
    const state = { "1": 2, "2": 1, "3": 1 };
    const newState = basketReducer(state, { type: DELETE_PRODUCT_IN_BASKET });
    expect(newState).toEqual({});
  });
});