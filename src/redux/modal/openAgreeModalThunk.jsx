import { CHANGE_CHOOSE_PRODUCT, OPEN_MODAL_AGREE } from "../actions"

export function openAgreeModalThunk(product){
    return function(dispatch){
        dispatch({type:CHANGE_CHOOSE_PRODUCT, payload:{chooseProduct:product}})
        dispatch({type: OPEN_MODAL_AGREE})
    }
}