import { modalReducer } from './modalReducer';
import { OPEN_MODAL_AGREE, CLOSE_MODAL_AGREE, OPEN_MODAL_DELETE, CLOSE_MODAL_DELETE } from '../actions';

describe('modalReducer', () => {
  it('should return the initial state', () => {
    const expectedState = { modalAgreeVisible: false, modalDeleteVisible: false };
    const newState = modalReducer(undefined, {});
    expect(newState).toEqual(expectedState);
  });

  it('should handle OPEN_MODAL_AGREE', () => {
    const expectedState = { modalAgreeVisible: true, modalDeleteVisible: false };
    const newState = modalReducer(undefined, { type: OPEN_MODAL_AGREE });
    expect(newState).toEqual(expectedState);
  });

  it('should handle CLOSE_MODAL_AGREE', () => {
    const expectedState = { modalAgreeVisible: false, modalDeleteVisible: false };
    const newState = modalReducer({ modalAgreeVisible: true, modalDeleteVisible: false }, { type: CLOSE_MODAL_AGREE });
    expect(newState).toEqual(expectedState);
  });

  it('should handle OPEN_MODAL_DELETE', () => {
    const expectedState = { modalAgreeVisible: false, modalDeleteVisible: true };
    const newState = modalReducer(undefined, { type: OPEN_MODAL_DELETE });
    expect(newState).toEqual(expectedState);
  });

  it('should handle CLOSE_MODAL_DELETE', () => {
    const expectedState = { modalAgreeVisible: false, modalDeleteVisible: false };
    const newState = modalReducer({ modalAgreeVisible: false, modalDeleteVisible: true }, { type: CLOSE_MODAL_DELETE });
    expect(newState).toEqual(expectedState);
  });
});