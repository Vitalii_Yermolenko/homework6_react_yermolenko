import React, { useEffect } from 'react'
import { Outlet } from 'react-router-dom';
import {Header} from '../components/Header';
import { Button } from '../components/Button';
import { Modal } from '../components/Modal';
import {ModalAgreeButtonBlock} from '../style/StyleComponents';
import { useDispatch,useSelector } from 'react-redux';
import { getProductsThunk } from '../redux/products/getProductsThunk';
import { chooseProductSelector, modalAgreeSelector, modalDeleteSelector } from '../redux/selectors';
import { addBasketThunk } from '../redux/basket/addBasketThunk';
import { removeBasketThunk } from '../redux/basket/removeBasketThunk';
import { CLOSE_MODAL_AGREE, CLOSE_MODAL_DELETE } from '../redux/actions';




export function Main() {
    const dispatch = useDispatch();
    const modalAgreeVisible = useSelector(modalAgreeSelector);
    const modalDeleteVisible = useSelector(modalDeleteSelector);
    const productChoose = useSelector(chooseProductSelector);

    useEffect(() => {
        dispatch(getProductsThunk());
      }, [dispatch]);

    return (
        <div>
            <Header/>
            
            <Outlet />

            {modalAgreeVisible && (
            <Modal
                className='modal-buy'
                header='Good choice!'
                text={`Do you want to buy "${productChoose.name}" ?`}
                onClose={() => dispatch({type: CLOSE_MODAL_AGREE})}
                actions={
                <ModalAgreeButtonBlock className="buttons">
                    <Button
                    backgroundColor='green'
                    text='Yes!'
                    handleClick={() => {
                        dispatch(addBasketThunk(productChoose.id));
                    }}
                    />
                    <Button
                    text='No'
                    backgroundColor='red'
                    handleClick={() => {
                        dispatch({type: CLOSE_MODAL_AGREE});
                    }}
                    />
                </ModalAgreeButtonBlock>
                }
            />
            )}   


            {modalDeleteVisible && (
            <Modal
                className='modal-delete'
                header='Delete product with basket'
                text={`Do you want to delete "${productChoose.name}" with basket?`}
                onClose={() => dispatch({type: CLOSE_MODAL_DELETE})}
                actions={
                <ModalAgreeButtonBlock className="buttons">
                    <Button
                    backgroundColor='red'
                    text='Yes!'
                    handleClick={() => {
                        dispatch(removeBasketThunk(productChoose.id));
                    }}
                    />
                    <Button
                    text='No'
                    backgroundColor='green'
                    handleClick={() => {
                        dispatch({type: CLOSE_MODAL_DELETE});
                    }}
                    />
                </ModalAgreeButtonBlock>
                }
            />
            )}           
        </div>

    )
}