import React ,{ useContext, useState } from 'react';
import { useSelector } from 'react-redux';
import { ProductsList } from '../components/ProductsList';
import { productsIsFavoriteSelector } from '../redux/selectors';
import { PageWrapper, PageTitle,StoreButtonWrapper } from "../style/StyleComponents.jsx";
import { DisplayModeContext } from '../context/StoreDisplayMode';
import { ProductsTable } from '../components/ProductTable';
import { Button } from '../components/Button';





export function Store() {
  const [displayMode, setDisplayMode] = useState('card'); 
  const productsWithFavorites = useSelector(productsIsFavoriteSelector);
  const { setDisplayMode: setDisplayModeContext } = useContext(DisplayModeContext);


  const handleDisplayModeClick = (newMode) => {
    if (newMode !== displayMode) {
      setDisplayMode(newMode); 
      setDisplayModeContext(newMode); 
    }
  };

  return (
    <PageWrapper>
      <PageTitle>Store</PageTitle>
          <StoreButtonWrapper>
          <Button 
            text='Cards' 
            className={`button-context ${displayMode === 'card' ? 'active' : ''}`}
            handleClick={() => {
                handleDisplayModeClick('card')
            }}
            backgroundColor=""
          />
            <Button 
            text='Table' 
            className={`button-context ${displayMode === 'table' ? 'active' : ''}`}
            handleClick={() => {
              handleDisplayModeClick('table')
            }}
            backgroundColor=''
          />
          </StoreButtonWrapper>
          {displayMode==='card' && <ProductsList products={productsWithFavorites}/>}
          {displayMode==='table' && <ProductsTable products={productsWithFavorites}  />}
    </PageWrapper>
  );
}