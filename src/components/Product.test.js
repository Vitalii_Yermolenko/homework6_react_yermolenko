import React from "react";
import renderer from "react-test-renderer";
import { Product } from "./Product";
import { Provider } from 'react-redux';
import configureMockStore from 'redux-mock-store';

const mockStore = configureMockStore();

describe('Product', () => {
  it('renders correctly', () => {
    const store = mockStore({}); 
    const product = {
      name: 'test',
      image: 'test',
      price: 10,
      article: 'test',
      color: 'red',
      isFavorite: false,
      id: 'test-id'
    };
    const tree = renderer
      .create(
        <Provider store={store}>
          <Product product={product} />
        </Provider>
      )
      .toJSON();
    expect(tree).toMatchSnapshot();
  });
});