import React from "react"
import { Button } from "./Button"
import { Wrapper,ModalAgree,ModalAgreeHeader } from "../style/StyleComponents";
import PropTypes from "prop-types";


export function Modal(props){
        return(
            <Wrapper 
            onClick={() => {
                props.onClose()
            }}>
                <ModalAgree
                onClick={(e) => {
                    e.stopPropagation();
                }}>
                <ModalAgreeHeader className={`${props.className}__header`}>
                    <h1 className={`${props.className}__title`}>{props.header}</h1>
                    <Button className={`${props.className}__close`} text='X' handleClick={(e) =>{
                        props.onClose();
                    }}/>
                </ModalAgreeHeader>
                <p className={`${props.className}__text`}>{props.text}</p>
                {props.actions}
                </ModalAgree>
            </Wrapper>
        )
     }


Modal.propTypes = {
    className:PropTypes.string,
    header:PropTypes.string,
    onClose: PropTypes.func,
}