import React from "react";
import { render, screen, fireEvent } from "@testing-library/react";
import { Modal } from "./Modal";

describe("Modal", () => {
  const mockHandleClose = jest.fn();
  const defaultProps = {
    className: "modal-class",
    header: "Modal Header",
    text: "Modal Text",
    onClose: mockHandleClose,
  };

  it("should render header and text", () => {
    render(<Modal {...defaultProps} />);
    expect(screen.getByText(defaultProps.header)).toBeInTheDocument();
    expect(screen.getByText(defaultProps.text)).toBeInTheDocument();
  });

  it("should handle close events", () => {
    render(<Modal {...defaultProps} />);
    const closeButton = screen.getByRole("button", { className: `${defaultProps.className}__close` });
    fireEvent.click(closeButton);
    expect(mockHandleClose).toHaveBeenCalled();
  });
});