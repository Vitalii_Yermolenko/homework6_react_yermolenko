import React from "react";
import { render, fireEvent } from "@testing-library/react";
import {Button} from "./Button"

describe("Button", () => {
  const mockHandleClick = jest.fn();
  const defaultProps = {
    className: "button-class",
    text: "Button Text",
    backgroundColor: "blue",
    handleClick: mockHandleClick,
  };

  it("should render button with text and background color", () => {
    const { getByRole } = render(<Button {...defaultProps} />);
    const button = getByRole("button");
    expect(button.textContent).toEqual(defaultProps.text);
    expect(button).toHaveStyle(`background-color: ${defaultProps.backgroundColor}`);
  });

  it("should handle click events", () => {
    const { getByRole } = render(<Button {...defaultProps} />);
    const button = getByRole("button");
    fireEvent.click(button);
    expect(mockHandleClick).toHaveBeenCalled();
  });
});