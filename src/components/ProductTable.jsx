import React from "react";
import PropTypes from "prop-types";
import { Button } from "./Button"
import { useDispatch } from "react-redux";
import { openAgreeModalThunk } from "../redux/modal/openAgreeModalThunk";
import { toogleFavoritesThunk } from "../redux/favorites/toogleFavoritesThunk";
import styled from "styled-components";


const StyledTable = styled.table`
  width: 100%;
  border-collapse: collapse;
  margin-top: 20px;

  th {
    text-align: left;
    background-color: #f7f7f7;
    border-bottom: 1px solid #ddd;
    padding: 8px;
    font-weight: bold;
    text-transform: uppercase;
  }

  tr {
    border-bottom: 1px solid #ddd;
    &:hover {
      background-color: #f5f5f5;
    }
  }

  td {
    text-align: center;
  }

  .color {
    display: flex;
    align-items: center;

    & p {
      margin-left: 10px;
      border-radius: 50%;
      width: 20px;
      height: 20px;
    }
  }

  .button {
    margin: 0 5px;
  }
`;

const FieldColor = styled.td`
display:flex;
justify-content: space-between;
`

export function ProductsTable({products }) {

const dispatch = useDispatch();
    
  if(!products){
    return(
      <div>No products</div>
    )
  }

  return (
    <div>
      <StyledTable>
        <thead>
          <tr>
            <th>Номер п/п</th>
            <th>Назва</th>
            <th>Артикул</th>
            <th>Колір</th>
            <th>Ціна</th>
          </tr>
        </thead>
        <tbody>
          {products.map((product, index) => {
            return (
              <tr key={product.article}>
                <td>{index + 1}</td>
                <td>{product.name}</td>
                <td>{product.article}</td>
                <FieldColor>{product.color}         
                    <p style={{
                        backgroundColor:product.color,
                        borderRadius: '50%',
                        width: '20px',
                        height: '20px',
                        }}>
                    </p>
                </FieldColor>
                <td>{product.price}</td>
                <td>
                    <Button 
                        text='Buy' 
                        className='button-buy'
                        handleClick={() => {
                        dispatch(openAgreeModalThunk(product))
                        }}
                        backgroundColor='#3A93FF'
                    />
                </td>
                <td>
                    <svg         
                        className='button-favorite'
                        onClick={() =>{
                        dispatch(toogleFavoritesThunk(product.id))
                        } }
                        viewBox="0 0 30 30"
                        width="40"
                        height="40"
                        fill={product.isFavorite ? "green" : "none"}
                        stroke="currentColor"
                        strokeWidth="1"
                        strokeLinecap="round"
                        strokeLinejoin="round">
                        <path d="M12 2 L15.09 8.45 L22 9.55 L17 14.24 L18.18 21.01 L12 17.77 L5.82 21.01 L7 14.24 L2 9.55 L8.91 8.45 Z" />
                    </svg>
                </td>
              </tr>
            );
          })}
        </tbody>
      </StyledTable>
    </div>
  );
}

ProductsTable.propTypes = {
  products: PropTypes.array,
}