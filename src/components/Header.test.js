import React from 'react';
import { render } from '@testing-library/react';
import { BrowserRouter } from 'react-router-dom'; 
import { Provider } from 'react-redux';
import { store } from '../redux/store';
import { Header } from './Header';

test('renders Header component', () => {
  const { asFragment } = render(
    <BrowserRouter>
      <Provider store={store}>
        <Header />
      </Provider>
    </BrowserRouter>,
  );
  expect(asFragment()).toMatchSnapshot();
});