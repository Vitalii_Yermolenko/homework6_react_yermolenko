import styled from "styled-components";


export const WrapperForm = styled.div`
padding: 10px 32px;
background-color: rgb(37,194,225);
border-radius: 30px;
`

//Header
export const WrapperChooseMenu = styled.div`
display:flex;
justify-content:center;
align-items: center;
gap:4px;
font-size:20px;
`
export const HeaderStyle = styled.header`
padding: 0 32px;
display:flex;
justify-content: space-between;
&::before {
    content: '';
    display: block;
    width: 100%;
    height: 4px;
    background-color: green;
    position: absolute;
    top: 108px;
    left: 0;
`


export const Navbar = styled.ul`
  width: 70%;
  margin-bottom: 40px;
  display: flex;
  list-style: none;
  font-size: 20px;
  text-transform: uppercase;
  justify-content: space-between;
  align-items: center;
  a {
    text-decoration: none;
    color: #000000;
    transition: 0.2s ease;
  }
  
  a:hover{
    border-bottom: 1px solid green;
    padding-bottom: 10px;
    box-shadow: 0px 18px 33px -27px rgba(0,0,0,0.75);
  }
  a.active-link{
    color: green;
  }
  }
`
export const PageWrapper = styled.div`
display: flex;
flex-direction: column;
justify-content: center;
align-items: center;
`

export const ErrorPageWrapper = styled.div`
position:absolute;
top:50%;
left:50%;
left: 50%;
transform: translate(-50%, -50%) }
display: flex;
flex-direction: column;
justify-content: center;
align-items: center;
`

export const PageTitle = styled.h2`
font-family: 'Montserrat';
font-style: normal;
font-weight: 700;
font-size: 22px;
line-height: 22px;
text-transform: uppercase;
`


//List
export const CardsWrapper = styled.div`
  width: fit-content;
  display: flex;
  flex-wrap: wrap;
  justify-content: center;
  gap: 30px;
  margin: 20px;
  `


  export const BasketCardBlock = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  pading: 8px;
  `

// Modal

export const Wrapper = styled.div`
    background-color: rgba(0, 0, 0, 0.1);
    position: fixed;
    display: flex;
    justify-content: center;
    align-items: center;
    width: 100%;
    z-index: 3;
    inset: 0px;
`



export const ModalAgree = styled.div`
    font-family: "HelveticaNeue";
    color: #D4FEF0;
    background-color: #3F0899;
    width: 600px;
    height: 300px;   
    display: flex;
    flex-direction: column;
    align-items: center;
    border: 3px black solid;
    border-radius: 12px;
    p{
        font-size: 22px;
        margin-top: 40px;
    }
`
export const ModalAgreeHeader = styled.header`
    font-size: 22px;
    text-align: left;
    background-color: #690DFF;
    width: 100%;
    height: 68px;
    display: flex;
    justify-content: space-around;
    align-items: center;
    font-weight: bold;

    button{
    background: none;
    color: #fff;
    font-size: 22px;
    border-radius: 50%;
    }
`


export const ModalAgreeButtonBlock = styled.div`
    display: flex;
    justify-content: center;
    align-items: center;
    margin-top: 50px;
    gap:50px;
    button{
        width: 100px;
        height: 60px;
        font-size: 22px;
        border-radius:25%;
    }

`

export const Card = styled.div`
width: 350px;
height: 620px;
display:flex;
flex-direction:column;
padding:10px;
border: 2px solid #1095C2;
border-radius:20px;
background-color:#C2AB9B;
img{
    height:65%;
}
`

export const CardText = styled.div`
display:flex;
flex-direction:column;
gap:8px;
align-items:left;
margin-top:8px;
p{
    margin:0;
}
`

export const CardButtons = styled.div`
margin-top:8px;
display:flex;
flex-direction:column;
align-items:right;
gap:10px
`

export const FormButtonCloseWrapper = styled.div`
font-size: 16px;
width: 100%;
display: flex;
justify-content: right;
align-items: center;
font-weight: bold;
padding: 8px 0;

button{
background: none;
color: #fff;
font-size: 22px;
border-radius: 50%;
}

`

export const StoreButtonWrapper = styled.div`
font-size: 16px;
width: 100%;
display: flex;
justify-content: center;
align-items: center;
font-weight: bold;
padding: 8px 0;

button{
background: #3A93FF;
color: #fff;
font-size: 22px;
}

button.active{
  background: black;
}
`
