import { createContext } from 'react';

export const DisplayModeContext = createContext({
  displayMode: 'card',
  setDisplayMode: () => {},
});